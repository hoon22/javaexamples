package practice1;

public class Main {
    /**
     * getGCD1:
     *  get greatest common divider between 2 integer using modular operator
     * @param num1  int
     * @param num2  int
     * @return int  GCD
     */
    public static int getGCD1(int num1, int num2) {
        while (true) {
            if (num1 >= num2) {
                if (num1 % num2 == 0)
                    return num2;
                num1 = num1 % num2;
            } else {
                if (num2 % num1 == 0)
                    return num1;
                num2 = num2 % num1;
            }
        }
    }

    /**
     * getGCD2:
     *  get greatest common divider between 2 integer using subtraction
     * @param num1  int
     * @param num2  int
     * @return int  GCD
     */
    public static int getGCD2(int num1, int num2) {
        while (true) {
            if (num1 == num2) {
                break;
            } else if (num1 < num2) {
                num2 = num2 - num1;
            } else {
                num1 = num1 - num2;
            }
        }
        return num1;
    }

    /**
     * convertDec2Hex: convert decimal number to string of hex number
     * @param input int (decimal number)
     * @return String   string of hex number
     */
    public static String convertDec2Hex(int input) {
        String hexResult = "";

        while (true) {
            int remainder = input % 16;
            char hexDigit = (char)(remainder < 10 ? '0' + remainder : 'A' + remainder - 10);

            hexResult = hexDigit + hexResult;

            input = input / 16;
            if (input < 1) break;
        }
        return hexResult;
    }

    /**
     * convertHex2Dec: convert string of hex number to decimal number
     * @param hexStr String
     * @return int  decimal number
     */
    public static int convertHex2Dec(String hexStr) {
        int decimalValue = 0;

        for (int i=0; i < hexStr.length(); i++) {
            int digit = hexStr.charAt(i) <= '9' ? hexStr.charAt(i) - '0' : hexStr.charAt(i) - 'A' + 10;
            decimalValue = decimalValue * 16 + digit;
        }
        return decimalValue;
    }

    /**
     * isPalindrome: check if input string is palindrome or not
     * @param strInput String
     * @return boolean  if palindrome return true, otherwise false
     */
    public static boolean isPalindrome(String strInput) {
        int lowIndex = 0;
        int highIndex = strInput.length() - 1;

        while (true) {
            if (lowIndex >= highIndex) {
                return true;
            }
            if (strInput.charAt(lowIndex) == strInput.charAt(highIndex)){
                lowIndex++;
                highIndex--;
                continue;
            }
            break;
        }
        return false;
    }

    /**
     * isPrime: check if a given number is prime number or not
     * @param value int
     * @return boolean  if input value is a prime number, return true. otherwise false
     */
    static boolean isPrime(int value) {
        if (value == 1) return false;   // 1 is not prime number
        if (value == 2) return true;    // 2 is prime number

        if (value % 2 == 0) return false;   // all even numbers are not prime number

        // check only for odd number
        int divNum = 3;
        while (divNum <= value / 2) {
            if (value % divNum == 0) {
                return false;
            }
            divNum += 2;
        }
        return true;
    }

    public static void main(String[] args) {
        // GCD Test
        int num1 = 45;
        int num2 = 20;
        System.out.printf("GCD(%d, %d) is %d\n", num1, num2, getGCD1(num1, num2));
//        System.out.printf("GCD(%d, %d) is %d\n", num1, num2, getGCD2(num1, num2));

        // Test: Decimal to Hex, Hex to Decimal
//        int input = 15556;
//        String hexStr = convertDec2Hex(input);
//        System.out.printf("Dec:%d, Hex:%s\n", input, hexStr);
//        System.out.printf("Hex:%s, Dec:%d\n", hexStr, convertHex2Dec(hexStr));


        // Palindrome
//        String inputStr1 = "ABCDEEDCBA";
//        System.out.printf("%s\n", (isPalindrome(inputStr) ? "Palindrome word" : "Not Palindrome word"));
//        inputStr1 = "This is test";
//        System.out.printf("%s\n", (isPalindrome(inputStr) ? "Palindrome word" : "Not Palindrome word"));

        // Prime number
//         int value = 3;
//         System.out.printf("%d, %s\n", value, (isPrime(value) ? "Prime number" : "Not Prime number"));
//         value = 27;
//         System.out.printf("%d, %s\n", value, (isPrime(value) ? "Prime number" : "Not Prime number"));
    }
}
