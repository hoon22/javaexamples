package Day06Examples;

import java.util.Scanner;
import java.util.*;
import java.util.stream.*;

public class ArrayInput2D {
    public static void printArray2D(int[][] myArr) {
        System.out.println("----- Array -----");
        for (int i=0; i<myArr.length; i++) {
            for (int j=0; j<myArr[i].length; j++) {
                System.out.printf("%d ", myArr[i][j]);
            }
            System.out.println();
        }
    }

    public static double calculateDistance(int[] point1, int[] point2) {
        return Math.sqrt(Math.pow(point1[0] - point2[0], 2) + Math.pow(point1[1] - point2[1], 2));
    }

    public static void main(String[] args) {
        int [][] myArr;

        try (Scanner sc = new Scanner(System.in)) {
//            System.out.print("Enter number of rows: ");
//            int rowSize = sc.nextInt();
//
//            System.out.print("Enter number of columns: ");
//            int colSize = sc.nextInt();
//
//            myArr = new int[rowSize][colSize];
//
//            for (int i=0; i<myArr.length; i++) {
//                for (int j=0; j<myArr[i].length; j++) {
//                    System.out.printf("Enter value (%d, %d): ", i, j);
//                    myArr[i][j] = sc.nextInt();
//                }
//            }
//            printArray2D(myArr);

            System.out.print("Enter string: ");
            String inStr = sc.nextLine();

            Set<String> setTest = Arrays.asList(inStr.split(" ")).stream().collect(Collectors.toSet());
            System.out.println(setTest.size()+ " distinct words: " + setTest);


            Map<String, Integer> m = new HashMap<>();

            // Initialize frequency table from command line
            for (String a : inStr.split(" ")) {
                Integer freq = m.get(a);
                m.put(a, (freq == null) ? 1 : freq + 1);
            }
            System.out.println(m.size() + " distinct words:");
            System.out.println(m);
        }

        // Random shuffling
//        for (int row=0; row<myArr.length; row++) {
//            for (int col=0; col<myArr[0].length; col++) {
//                int newRow = (int) Math.random() * myArr.length;
//                int newCol = (int) Math.random() * myArr[0].length;
//
//                int temp = myArr[row][col];
//                myArr[row][col] = myArr[newRow][newCol];
//                myArr[newRow][newCol] = temp;
//            }
//        }
//        printArray2D(myArr);

//        System.out.println(calculateDistance(new int[] {1,2}, new int[] {2,3}));
    }
}
