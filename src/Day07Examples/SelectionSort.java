package Day07Examples;

import java.util.Arrays;

public class SelectionSort {
    static void sort(int[] arr, int curPos) {
        if (curPos < arr.length) {
            int smallestIndex = curPos;
            for (int i=curPos+1; i<arr.length; i++) {
                if (arr[smallestIndex] > arr[i]) {
                    smallestIndex = i;
                }
            }
            int temp = arr[smallestIndex];
            arr[smallestIndex] = arr[curPos];
            arr[curPos] = temp;

            sort(arr, ++curPos);
        }
    }

    public static void main(String argc[]) {
        int [] myArr = {2, 4, 5, 1, 3};
        sort(myArr, 0);
        System.out.println(Arrays.toString(myArr));
    }
}
