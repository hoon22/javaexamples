package Day07Examples;

import java.util.Scanner;

public class Palindrome {
//    static String myStr = "AACAA";

    static boolean isPalindrome(String myStr, int low, int high) {
        if (low >= high) return true;
        if (myStr.charAt(low) == myStr.charAt(high))
            return isPalindrome(myStr, low+1, high-1);
        return false;
    }

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            while (true) {
                System.out.println("Enter string to check palindrome (to exit enter");
                String myStr = sc.nextLine();
                if (myStr == null) break;

                System.out.println(isPalindrome(myStr,0, myStr.length() - 1));
            }
        }
    }
}
