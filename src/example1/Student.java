package example1;

public class Student extends Person {
    private String id;

    Student() {
//        System.out.println("Student was created");
    }

    Student(String name, String id) {
        super(name);
        this.id = id;
//        System.out.println("Student was created");
    }

    @Override
    public String toString() {
        return String.format("Student{ %s, id='%s' }", super.toString(), id);
    }
}
