package example1;

import java.util.regex.Pattern;

public class Person implements Language{
    Person () {
        this.name = "No name";
//        System.out.println("Person object was created");
    }

    Person (String name) {
        setName(name);
//        System.out.println("Person object was created");
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String pattern = "^([A-Za-z]+[,.]?[ ]?|[A-Za-z]+['-]?)+$";

        if (!Pattern.matches(pattern, name))
            throw new IllegalArgumentException("Illegal Name format: " + name);
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Person{name='%s'}", name);
    }

    @Override
    public void sayHello() {
        System.out.println("Annyung!");
    }
}
