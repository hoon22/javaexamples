package example1;

public class Main {
    public static void main(String[] args) {
        try {
            // Person p = new Person("James");
            Student s = new Student("James", "S1234");
            System.out.println(s);

            Teacher t = new Teacher("Jackson", "Mathematics");
            System.out.println(t);

        } catch (IllegalArgumentException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
