package example1;

public class Teacher extends Person {
    String subject;

    Teacher() {
//        System.out.println("Teacher created");
    }

    Teacher(String name, String subject) {
        super(name);
        setSubject(subject);
//        System.out.println("Teacher created");
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return String.format("Teacher{ %s, subject='%s' }", super.toString(), subject);
    }
}
